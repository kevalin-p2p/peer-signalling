// Package dht represents a server-based fallback for the Kevalin DHT. It is used for
// bootstrapping the network and persisting data when not many nodes are available.
package dht

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/mux"
)

const expireSeconds = 60 * 60

var mutex sync.RWMutex
var cache = map[string]map[string]*Record{}

// Record is the top-level type stored in the hash table
type Record struct {
	ID        string `dynamo:"id" json:"id"`
	Timestamp int64  `dynamo:"timestamp" json:"timestamp"`
	Expires   int64  `dynamo:"expires" json:"expires"`
	Value     *Value `dynamo:"value" json:"value"`
}

// Value is the specific payload stored in the hash table, with authorship and PoW assertions
type Value struct {
	Payload   *json.RawMessage `dynamo:"payload" json:"payload"`
	Hash      string           `dynamo:"hash" json:"hash"`
	Nonce     int64            `dynamo:"nonce" json:"nonce"`
	Identity  string           `dynamo:"identity" json:"identity"`
	Signature string           `dynamo:"signature" json:"signature"`
}

// Setup registers the /dht endpoint handlers
func Setup(r *mux.Router) {
	r.HandleFunc("/dht/{id}", handlePut).Methods("PUT")
	r.HandleFunc("/dht/{id}", handleGet).Methods("GET")

	go cleanup()
}

func handlePut(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	// TODO validate ID

	// TODO limit size of body
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Read: "+err.Error(), http.StatusInternalServerError)
		return
	}

	var value *Value
	err = json.Unmarshal(body, value)
	if err != nil {
		http.Error(w, "Unmarshal: "+err.Error(), http.StatusInternalServerError)
		return
	}

	// TODO verify payload hash
	// TODO validate signature
	// TODO validate proof of work
	record := &Record{
		ID:        id,
		Timestamp: time.Now().Unix(),
		Expires:   time.Now().Unix() + expireSeconds,
		Value:     value,
	}

	err = put(record)
	if err != nil {
		http.Error(w, "Put: "+err.Error(), http.StatusInternalServerError)
		return
	}

	_, err = w.Write([]byte(`"OK"`))
	if err != nil {
		http.Error(w, "Response: "+err.Error(), http.StatusInternalServerError)
		return
	}
}

func handleGet(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	// TODO validate ID

	values, err := get(id)
	if err != nil {
		http.Error(w, "Get: "+err.Error(), http.StatusInternalServerError)
		return
	}

	response, err := json.Marshal(values)
	if err != nil {
		http.Error(w, "Marshal: "+err.Error(), http.StatusInternalServerError)
		return
	}

	_, err = w.Write(response)
	if err != nil {
		http.Error(w, "Response: "+err.Error(), http.StatusInternalServerError)
		return
	}

}

func put(r *Record) error {
	mutex.Lock()
	log.Printf("Put %s:%s", r.ID, r.Value.Hash)
	entries := cache[r.ID]
	if entries == nil {
		entries = map[string]*Record{}
	}
	entries[r.Value.Hash] = r
	mutex.Unlock()
	return nil
}

func get(id string) ([]*Record, error) {
	mutex.RLock()
	log.Printf("Get %s", id)
	values := []*Record{}
	for _, value := range cache[id] {
		values = append(values, value)
	}
	mutex.RUnlock()
	return values, nil
}

func remove(id, hash string) error {
	mutex.Lock()
	log.Printf("Removing %s:%s", id, hash)
	entries := cache[id]
	if entries != nil {
		delete(entries, hash)
	}
	if len(entries) == 0 {
		delete(cache, id)
	}
	mutex.Unlock()
	return nil
}

func cleanup() {
	for {
		<-time.After(time.Second * expireSeconds)

		expireBefore := time.Now().Add(-time.Second * expireSeconds)
		log.Println("Scanning for expired entries inserted before", expireBefore)

		mutex.RLock()
		for id, entries := range cache {
			for hash, entry := range entries {
				if entry.Expires < time.Now().Unix() {
					go remove(id, hash)
				}
			}
		}
		mutex.RUnlock()
	}
}
