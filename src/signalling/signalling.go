package signalling

import (
	"encoding/json"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/mux"

	"github.com/gorilla/websocket"
)

const expireSeconds = 120
const maxAnnouncements = 10

// Register tells the server to register the given peer in the active peers list
type Register struct {
	Peer string `json:"peer"`
}

// Signal requests that a message is delivered to another peer
type Signal struct {
	Peer      string          `json:"peer"`
	Sender    string          `json:"sender"`
	Offer     json.RawMessage `json:"offer"`
	Answer    json.RawMessage `json:"answer"`
	Candidate json.RawMessage `json:"candidate"`
	Heartbeat int             `json:"heartbeat"`
}

// Peer holds the connection to a peer and information about its status
type Peer struct {
	sync.Mutex
	*websocket.Conn
	ID       string
	lastSeen time.Time
}

var peers = map[string]*Peer{}
var notify = make(chan Signal)
var upgrader = websocket.Upgrader{
	CheckOrigin: checkOrigin,
}
var mutex sync.RWMutex

func checkOrigin(r *http.Request) bool {
	return true
}

// Setup registers the /signalling endpoints and starts background processing
func Setup(r *mux.Router) {
	r.HandleFunc("/signalling/ws", handleConnections)
	go cleanup()
}

func handleConnections(w http.ResponseWriter, r *http.Request) {
	// Upgrade initial GET request to a websocket
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal(err)
	}
	// Make sure we close the connection when the function returns
	defer ws.Close()
	log.Println("Opened connection")

	var reg Register
	err = ws.ReadJSON(&reg)
	if err != nil {
		log.Printf("error: %v", err)
		return
	}

	mutex.Lock()
	client := &Peer{
		Conn:     ws,
		ID:       reg.Peer,
		lastSeen: time.Now(),
	}
	peers[reg.Peer] = client
	mutex.Unlock()
	log.Println("Registered peer", reg.Peer)

	client.processSignals()
}

func (p *Peer) processSignals() {
	for {
		// Read in a new message as JSON and map it to a Message object
		msg := &Signal{}
		err := p.ReadJSON(msg)
		if err != nil {
			log.Printf("error: %v", err)
			mutex.Lock()
			delete(peers, p.ID)
			mutex.Unlock()
			log.Println("Removed peer", p.ID)
			break
		}
		msg.Sender = p.ID

		p.Lock()
		p.lastSeen = time.Now()
		p.Unlock()

		if msg.Heartbeat != 0 {
			// log.Printf("Received heartbeat from %s", reg.Peer)
			continue
		}
		log.Printf("Received signal for %s from %s", msg.Peer, p.ID)

		// Send the newly received signal to the requested peer(s)
		if msg.Peer == "announce" {
			p.announce(msg)
		} else {
			p.route(msg)
		}
	}
}

// Delivers an annoucement to K random peers
func (p *Peer) announce(msg *Signal) {
	mutex.RLock()
	log.Println("Announcing peer", p.ID)
	count := 0
	for id, peer := range peers {
		if id == p.ID {
			continue
		}

		peer.send(msg)

		count++
		if count == maxAnnouncements {
			break
		}
	}
	mutex.RUnlock()
}

// Delivers a message from a given peer
func (p *Peer) route(msg *Signal) {
	mutex.RLock()
	peer := peers[msg.Peer]
	mutex.RUnlock()
	if peer == nil {
		log.Println("error: peer not found")
		return
	}
	peer.send(msg)
}

// Sends a message to the given peer from a specific sender
func (p *Peer) send(msg *Signal) {
	p.Lock()
	log.Println("Sending signal")
	err := p.WriteJSON(msg)
	if err != nil {
		mutex.Lock()
		log.Printf("error: %v", err)
		p.Close()
		delete(peers, msg.Peer)
		log.Println("Removed peer", msg.Peer)
		mutex.Unlock()
	} else {
		p.lastSeen = time.Now()
	}
	p.Unlock()
}

func cleanup() {
	for {
		<-time.After(time.Second * expireSeconds)

		expireBefore := time.Now().Add(-time.Second * expireSeconds)
		log.Println("Scanning for expired peers last seen before", expireBefore)

		mutex.RLock()
		for _, peer := range peers {
			peer.Lock()
			if peer.lastSeen.Before(expireBefore) {
				peer.Close()
			}
			peer.Unlock()
		}
		mutex.RUnlock()
	}
}
