package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/gorilla/mux"
	"github.com/hashicorp/serf/serf"
	flags "github.com/jessevdk/go-flags"
	"github.com/rs/cors"

	"bitbucket.org/kevalin-p2p/peer-signalling/src/dht"
	"bitbucket.org/kevalin-p2p/peer-signalling/src/signalling"
)

// Options defines the command line configuration options for this service
type Options struct {
	Wait     time.Duration `short:"t" long:"graceful-timeout" default:"15s" description:"the duration for which the server gracefully waits for existing connections to finish - e.g. 15s or 1m"`
	Port     int           `short:"p" long:"port" env:"PORT" default:"8083" description:"the port to listen on for HTTP connections"`
	SerfPort int           `short:"s" long:"serf-port" env:"SERF_PORT" default:"7946" description:"the port to listen on for Serf connections"`
}

var o = &Options{}

func main() {
	flags.Parse(o)

	srv := setupHTTP()
	srf, err := setupSerf()
	if err != nil {
		log.Fatalf("Initialising Serf: %s", err)
	}
	log.Println("Serf: ", srf.State())

	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), o.Wait)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	srv.Shutdown(ctx)
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	log.Println("shutting down")
	os.Exit(0)
}

func setupHTTP() *http.Server {

	r := mux.NewRouter()

	fs := http.FileServer(http.Dir("./public"))
	r.Handle("/", fs)

	signalling.Setup(r)
	dht.Setup(r)

	handler := cors.Default().Handler(r)
	srv := &http.Server{
		Addr: fmt.Sprintf(":%d", o.Port),
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      handler,
	}

	// Run our server in a goroutine so that it doesn't block.
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Fatal("ListenAndServe: ", err)
		}
	}()
	log.Printf("http server started on http://localhost:%d", o.Port)

	return srv
}

func setupSerf() (*serf.Serf, error) {
	config := serf.DefaultConfig()

	srf, err := serf.Create(config)
	if err != nil {
		return nil, err
	}

	return srf, nil
}
